package GitLab;

use strict;
use warnings;

use Carp;
use Log::Any qw($log);
use Try::Tiny;

use GitLab::API;

use GitLab::Commits;
use GitLab::CustomAttributes;
use GitLab::Discussions;
use GitLab::DeployKeys;
use GitLab::Events;
use GitLab::Groups;
use GitLab::Jobs;
use GitLab::Members;
use GitLab::MergeRequests;
use GitLab::Namespaces;
use GitLab::Notes;
use GitLab::Pages;
use GitLab::Projects;
use GitLab::Repositories;
use GitLab::Runners;
use GitLab::Users;

use parent "Exporter";

our %EXPORT_TAGS = (
    group_visibility => [qw(
        GROUP_VISIBILITY
        GROUP_PRIVATE
        GROUP_INTERNAL
        GROUP_PUBLIC
    )],

    member_access => [qw(
        MEMBER_ACCESS
        MEMBER_GUEST
        MEMBER_REPORTER
        MEMBER_DEVELOPER
        MEMBER_MASTER
        MEMBER_MAINAINER
        MEMBER_OWNER
    )],

    project_visibility => [qw(
        PROJECT_PRIVATE
        PROJECT_INTERNAL
        PROJECT_PUBLIC
    )],

    utils => [qw(
        group_visibility_name
        member_access_name
    )],
);

Exporter::export_ok_tags(qw(
    group_visibility
    member_access
    project_visibility utils
));

#===============================================================================
#   Constants
#===============================================================================

use constant {
    # Group Visibility
    GROUP_PRIVATE => "private",
    GROUP_INTERNAL => "internal",
    GROUP_PUBLIC => "public",

    # Member rights
    MEMBER_GUEST => 10,
    MEMBER_REPORTER => 20,
    MEMBER_DEVELOPER => 30,
    MEMBER_MAINTAINER => 40,
    MEMBER_OWNER => 50,

    # Project Visibility
    PROJECT_PRIVATE => "private",
    PROJECT_INTERNAL => "internal",
    PROJECT_PUBLIC => "public",
};

sub MEMBER_MASTER {
    carp "MEMBER_MASTER is deprecated, use MEMBER_MAINTAINER instead";
    return MEMBER_MAINTAINER;
}

use constant GROUP_VISIBILITY => (
    GROUP_PRIVATE,
    GROUP_INTERNAL,
    GROUP_PUBLIC,
);

use constant MEMBER_ACCESS => (
    MEMBER_GUEST,
    MEMBER_REPORTER,
    MEMBER_DEVELOPER,
    MEMBER_MAINTAINER,
    MEMBER_OWNER,
);

use constant PROJECT_VISIBILITY => (
    PROJECT_PRIVATE,
    PROJECT_INTERNAL,
    PROJECT_PUBLIC,
);

#===============================================================================
#   Utilities
#===============================================================================

my %group_visibility_map = (
    "10" => "Private",
    "20" => "Internal",
    "30" => "Public",
);

sub group_visibility_name {
    my ($code) = @_;
    return $group_visibility_map{$code} // "Invalid";
}

my %member_access_map = (
    "10" => "Guest",
    "20" => "Reporter",
    "30" => "Developer",
    "40" => "Master",
    "50" => "Owner",
);

sub member_access_name {
    my ($code) = @_;
    return $member_access_map{$code} // "Invalid";
}

my %_project_visibility_map = (
     "0" => "Private",
    "10" => "Internal",
    "20" => "Public",
);

sub _project_visibility_name {
    my ($code) = @_;
    return $_project_visibility_map{$code} // "Invalid";
}

my %project_visibility_map = (
    "private" => "Private",
    "internal" => "Internal",
    "public" => "Public",
);

sub project_visibility_name {
    my ($code) = @_;
    return $project_visibility_map{$code} // "Invalid";
}

1;

__END__

=head1 NAME

    GitLab - convenience wrapper for GitLab::API that provides useful constants

=head1 SYNOPSIS

    use GitLab qw(:utils);

    my $gitlab = GitLab::API->new(...);
    my $users  = $gitlab->users();

=head1 DESCRIPTION

=head2 Constants

The module defines access level constants. See appropriate API documentation
for explanation.

=head3 Group visibility

Import with C<:group_visibility> tag.

    10 GROUP_PRIVATE
    20 GROUP_INTERNAL
    30 GROUP_PUBLIC

=head3 Member access

Import with C<:member_access> tag.

    10 MEMBER_GUEST
    20 MEMBER_REPORTER
    30 MEMBER_DEVELOPER
    40 MEMBER_MAINTAINER
    50 MEMBER_OWNER

There is also a deprecated C<MEMBER_MASTER> level with the same value as
C<MEMBER_MAINTAINER>. It will be removed from future releases of this API.

=head3 Project Visibility

Import with C<:project_visibility> tag.

     0 PROJECT_PRIVATE
    10 PROJECT_INTERNAL
    20 PROJECT_PUBLIC

=head2 Utilities

Import with C<:utils> tag.

=over

=item group_visibility_name()

    $name = GitLab::group_visibility_name($code);

Translates a Group Visibility code to its string representation, i.e. I<Private>, I<Internal> or I<Public>.
If the parameter does not represent a valid GitLab Group Visibility code, returns I<Invalid>.

=item member_access_name()

    $name = GitLab::member_access_name($code);

Translates a Member Access code to its string representation, i.e. I<Guest>, I<Reporter>, I<Developer>, I<Master> and I<Owner>.
If the parameter does not represent a valid GitLab Member Access code, returns I<Invalid>.

=item project_visibility_name()

    $name = GitLab::project_visibility_name($code);

Translates a Project Visibility code to its string representation, i.e. I<Private>, I<Internal> and I<Public>.
If the parameter does not represent a valid GitLab Project Visibility code, returns I<Invalid>.

=back

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab::API::v3>

Complete GitLab API v3 implementation with CLI support.

=item L<GitLab::API>

implementation of the L<GitLab API|http://docs.gitlab.com/ce/api/> client.

=item L<GitLab::Groups>

L<GitLab Groups|http://docs.gitlab.com/ce/api/groups.html> methods.

=item L<GitLab::Namespaces>

L<GitLab Namespaces|http://docs.gitlab.com/ce/api/namespaces.html> methods.

=item L<GitLab::Users>

L<GitLab Users|http://docs.gitlab.com/ce/api/users.html> methods.

=item L<GitLab::Projects>

L<GitLab Users|http://docs.gitlab.com/ce/api/projects.html> methods.

=back
