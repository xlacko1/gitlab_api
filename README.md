# GitLab::API

Lightweight implementation of [GitLab CE API](https://docs.gitlab.com/ce/api/) in Perl.

## Quick start

```perl
use GitLab;

my $gitlab = GitLab::API->new(
    URL        => "https://gitlab.fi.muni.cz/api/v3",
    AuthToken  => "...",
    DieOnError => 1
);

my $users  = $gitlab->users(-iterator => 1);

say "Users in GitLab:";
while (my $user = $users->next) {
    say $user->{name};
}

my $groups = $gitlab->groups(search => "pb161");
```

## Documentation

- [official GitLab API documentation](https://docs.gitlab.com/ce/api/)
- [`GitLab::API` wiki](https://gitlab.fi.muni.cz/xlacko1/gitlab_api/wikis/home)

## Dependencies

- [`HTTP::Headers`](https://metacpan.org/pod/HTTP::Headers)
- [`JSON`](https://metacpan.org/pod/JSON)
- [`Log::Any`](https://metacpan.org/pod/Log::Any)
- [`LWP::UserAgent`](https://metacpan.org/pod/LWP::UserAgent)
- [`LWP::Protocol::https`](https://metacpan.org/pod/LWP::Protocol::https)
- [`Try::Tiny`](https://metacpan.org/pod/Try::Tiny)
- [`URI`](https://metacpan.org/pod/URI)
- [`URI::QueryParam`](https://metacpan.org/pod/URI::QueryParam)

## Author

Roman Lacko <[`xlacko1@fi.muni.cz`](mailto:xlacko1@fi.muni.cz)>
