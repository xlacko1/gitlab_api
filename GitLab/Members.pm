package GitLab::Members;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    "<ENTITY>_members"   => {
        method      => "GET",
        path        => "/<ENTITY>s/<id>/members",
        paginated   => 1,
        optional    => [qw(
            query
        )],
    },

    "<ENTITY>_get_member" => {
        method      => "GET",
        path        => "/<ENTITY>s/<id>/members/<user_id>",
    },

    "<ENTITY>_add_member" => {
        method      => "POST",
        path        => "/<ENTITY>s/<id>/members",
        required    => [qw(
            user_id
            access_level
        )],
        optional    => [qw(
            expires_at
        )],
    },

    "<ENTITY>_update_member" => {
        method      => "PUT",
        path        => "/<ENTITY>s/<id>/members/<user_id>",
        required    => [qw(
            access_level
        )],
        optional    => [qw(
            expires_at
        )],
    },

    "<ENTITY>_update_member" => {
        method      => "PUT",
        path        => "/<ENTITY>s/<id>/members/<user_id>",
        required    => [qw(
            access_level
        )],
        optional    => [qw(
            expires_at
        )],
    },

    "<ENTITY>_delete_member" => {
        method      => "DELETE",
        path        => "/<ENTITY>s/<id>/members/<user_id>",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);

    my $emap = {
        group   => "gid",
        project => "id",
    };

    while (my ($name, $tmpl) = each(%$requests)) {
        foreach my $entity (keys %$emap) {
            my $copy = { %$tmpl };

            $copy->{name} = $name
                unless exists $copy->{name};

            foreach my $v (values %$copy) {
                $v =~ s/<ENTITY>/$entity/g;
                $v =~ s/<id>/<$emap->{$entity}>/g;
            }

            GitLab::API->register($copy);
        }
    }
}

1;

__END__

=head1 NAME

GitLab::Members - implements group and project members API calls

See L<GitLab API -- Group and Project Members|https://docs.gitlab.com/ce/api/members.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
