package GitLab::API::Guard;

sub new {
    my ($class, $clean) = @_;
    bless { _clean => $clean }, $class;
}

sub DESTROY {
    my ($self) = @_;
    $self->{_clean}->() if defined $self->{_clean};
}

1;
