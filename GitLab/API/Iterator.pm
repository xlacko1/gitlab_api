package GitLab::API::Iterator;

use utf8;
use strict;
use warnings;

use Carp            qw();
use HTTP::Status    qw(:constants);
use Log::Any        qw($log);

our $VERSION = 8.12.1;

sub carp(@)  { $log->warn(@_);  Carp::carp  @_; }
sub croak(@) { $log->fatal(@_); Carp::croak @_; }

#===============================================================================
#   Constructor
#===============================================================================

sub new {
    my ($class, $api, $url, %extra) = @_;

    my $self = {
        api         => $api,
        url         => $url,
        per_page    => $extra{per_page} // 50,
        tmpl        => $extra{template} // { name => "unknown" },
    };

    bless $self, $class;

    $self->idebug("created url=\"$url\", per_page=$self->{per_page}");
    $self->reset;

    return $self;
}

sub api     { return shift->{api};   }
sub url     { return shift->{url};   }
sub count   { return shift->{count}; }
sub name    { return shift->{tmpl}->{name}; }
sub pages   { return shift->{pages_read};   }

sub idebug  { $log->debug("<" . shift->name . "> " . join("", @_)); }

#===============================================================================
#   Public methods
#===============================================================================

sub next {
    my ($self) = @_;

    if ($self->{ix} + 1 == $self->count && ($self->{finished} || !$self->next_block)) {
        $self->idebug("already finished, nothing to do");
        return;
    }

    ++$self->{ix};
    return $self->current;
}

sub reset {
    my ($self) = @_;

    $self->idebug("resetting");
    $self->{data}           = [];
    $self->{ix}             = -1;
    $self->{count}          =  0;
    $self->{pages_read}     =  0;
    $self->{pages_total}    =  undef;
    $self->{finished}       =  0;
    $self->{good}           =  1;
    $self->{responses}      = [];
}

sub rewind {
    my ($self) = @_;

    $self->idebug("rewinding");
    $self->{ix} = -1;
}

sub current {
    my ($self) = @_;
    return $self->{ix} == -1 || !$self->{good}
        ? undef
        : $self->{data}->[$self->{ix}];
}

sub response {
    my ($self, $index) = @_;
    return !$self->{good}
        ? undef
        : $self->{responses}->[$index];
}

sub all {
    my ($self) = @_;
    return if !$self->{good};

    $self->idebug("retrieving all pages");
    while (!$self->{finished} && $self->next_block) {
        # nop
    }

    return !$self->{good} ? undef : $self->{data};
}

#===============================================================================
#   Internals
#===============================================================================

sub next_block {
    my ($self) = @_;
    my $api = $self->api;

    if (!$self->{good}) {
        carp "Called next_block on an invalid iterator for " . $self->name;
        return 0;
    }

    $self->idebug("requesting a new block, has=$self->{pages_read} total=" . ($self->{pages_total} // "unknown"));

    if (defined $self->{pages_total} && $self->{pages_total} == $self->{pages_read}) {
        $self->idebug("nothing to do");
        $self->{finished} = 1;
        return 0;
    }

    my $xurl = $self->{url}->clone;
    $xurl->query_param(page     => $self->{pages_read} + 1);
    $xurl->query_param(per_page => $self->{per_page});

    my %headers;
    my ($tag, $cached);
    my ($response, $data);

    # try using cache if defined
    if (defined $api->cache) {
        ($tag, $cached) = $api->cache->get($xurl);
        $headers{"If-None-Match"} = $tag if (defined $tag);
    }

    $self->idebug("GET " . $xurl);
    my $httpraw = $api->http->get($xurl, %headers);
    if (defined $api->cache && $httpraw->code == HTTP_NOT_MODIFIED) {
        ($response, $data) = ($httpraw, $cached);
    } else {
        $api->cache->flush($xurl) if defined $api->cache;
        ($response, $data) = $api->clean_data($httpraw);
    }

    if ($log->is_trace) {
        $log->trace("----  HTTP REQUEST  ".("-" x 25)."\n".$response->request->as_string);
        $log->trace("----  HTTP RESPONSE ".("-" x 25)."\n".$response->as_string);
        $log->trace("--------------------".("-" x 25));
    }

    $self->idebug("status: " . $response->status_line);

    if (!$response->is_success && $response->code != HTTP_NOT_MODIFIED) {
        $self->{good} = 0;

        croak "Cannot obtain next block for '" . $self->name . "': " . $response->status_line
            if $self->api->die_on_error;
        return 0;
    }

    if (defined $api->cache) {
        my $tag = $response->header("ETag");
        $api->cache->set($xurl, $tag, $data)
            if defined $tag;
    }

    croak "Response for " . $self->name . " did not contain an array"
        unless ref $data eq "ARRAY";

    # some operations do not know the number of pages beforehand;
    # thus we shall increase the numer as we get more and more pages
    if (!defined $response->header('X-Total-Pages') && defined $response->header('X-Next-Page')) {
        $self->{pages_total} = $response->header('X-Next-Page');
        $self->idebug("pages_total=$self->{pages_total} (assumed)");
    # get total number of pages from the header
    } elsif (!defined $self->{pages_total}) {
        # some requests are not paginated if additional parameters are provided
        # use 1 if there are data, 0 otherwise
        my $default = @$data ? 1 : 0;
        $self->{pages_total} = $response->header("X-Total-Pages") // $default;
        $self->idebug("pages_total=$self->{pages_total}");
    }

    push @{$self->{data}},      @$data;
    push @{$self->{responses}}, $response;

    $self->api->{last}  = $response;
    $self->{count}     += scalar @$data;

    # only increase read pages if there are pages to read
    ++$self->{pages_read} if $self->{pages_total};

    $self->{finished} = !$self->{pages_total} || $self->{pages_read} == $self->{pages_total};
    $self->idebug("block read successful");

    $self->idebug("finished")
        if $self->{finished};

    return 1;
}

1;

#===============================================================================
#   DOCUMENTATION
#===============================================================================

__END__

=head1 NAME

GitLab::API::Iterator - iterator for paginated responses

=head1 SYNOPSIS

    use GitLab::API;
    use GitLab::Users;

    my $gitlab = GitLab::API->new(...);

    my $users = $gitlab->users(-iterator => 1);

    while (my $user = $users->next) {
        # ...
    }

    print "Total users: ", $users->count, "\n";

    # ...
    $users->rewind;

    while (my $user = $users->next()) {
        # ...
    }

=head1 DESCRIPTION

The iterator provides methods C<next> and C<rewind>.
It also remembers pages that have already been downloaded.
When the iterator reaches the end of data, it tries to download the next page.

Note that all entities are memoized, so rewinding the iterator will B<not> cause it to request pages again.
Use B<reset> for this kind of behaviour.

=over

=item new()

    $iterator = GitLab::API::Iterator->new($api, $url, %extra)

Constructor.
Not intended to be called directly, L<GitLab::API/exec_request> constructs the instance itself.

Arguments:

=over

=item C<$api>

The L<GitLab::API> instance that will be used to issue calls.

=item C<$url>

L<URI> instance for the call.

=item C<%extra>

Extra arguments.
Currently only C<per_page> is used, which controls how many entities in a page can the iterator request.

=back

=item next()

Returns the next element or C<undef> if there are no elements left.
Automatically downloads next page from API if required.

=item reset()

    $iterator->reset

Resets the iterator to the original state that can be reused.
Clears all data and error flags, so that the requests can be issued again.

=item rewind()

    $iterator->rewind

Resets the iterator to the position before the first element, so that the call to C<next> will return the first element.
Note that this will B<not> cause the iterator to re-download all pages.
Use C<reset> for this purpose.

=item all()

    my @items = @{ $iterator->all };

Downloads all pages and returns an arrayref of all elements.

=back

=head2 Internal methods

=over

=item next_block()

Tries to download the next page from the GitLab API.
Returns false if no page left or an error occured.

=back

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab::API>

Lightweight GitLab API client.

=item L<GitLab>

    use GitLab;

Wrapper around L<GitLab::API> and other modules.

=back
