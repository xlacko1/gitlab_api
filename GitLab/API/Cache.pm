package GitLab::API::Cache;

use utf8;
use strict;
use warnings;

use Log::Any    qw($log);

sub new {
    my ($class) = @_;

    $log->info("caching is enabled");
    return bless {}, $class;
};

sub _uri_to_key {
    my (undef, $uri) = @_;
    return $uri->path . ":" . ($uri->query // "");
}

sub get {
    my ($self, $uri) = @_;

    my $key = $self->_uri_to_key($uri);
    my $p   = $self->{data}->{$key};

    if ($p) {
        $log->trace("cache seach hit '$key' etag '$p->[0]'");
        return @$p;
    }

    $log->trace("cache search miss '$key'");
    return;
};

sub set {
    my ($self, $uri, $tag, $data) = @_;

    my $key = $self->_uri_to_key($uri);
    $self->{data}->{$key} = [ $tag, $data ];

    $log->trace("cache store '$key' etag '$tag'");
    # don't leak data
    return;
};

sub flush {
    my ($self, @uris) = @_;
    my @keys = map { $self->_uri_to_key($_) } (grep { defined } @uris);

    if (!@uris) {
        $log->trace("cache flush all");
        $self->{data} = {};
    } else {
        $log->trace("cache flush " . join(",", map { "'$_'" } @keys))
            if $log->is_trace;
        delete $self->{data}->{@keys};
    }

    # don't leak old keys
    return;
}

1;

__END__

=head1 NAME

GitLab::API::Cache - ETag caching module

=head1 SYNOPSIS

    use GitLab;

    my $gitlab = GitLab::API->new(
        AuthToken   => $token,
        URL         => $url,
        # enable caching
        Cache       => 1,
    );

    # first request
    my $user = $gitlab->user;
    # result gets cached with ETag

    # ...

    # second request, if nothing changes, will use the cached ETag
    my $user2 = $gitlab->user;

=head1 DESCRIPTION

=head2 Overview

This module allows L<GitLab::API> to use ETag cache. It works like this:

=over

=item 1

A request is made, say, to L<https://HOST/api/v4/user>:

    GET https://${HOST}/api/v4/user
    User-Agent: libwww-perl/6.26
    PRIVATE-TOKEN: ${TOKEN}

=item 2

If everything goes OK, a HTTP response is returned, which may look like this:

    HTTP/1.1 200 OK
    Connection: keep-alive
    ...
    ETag: W/"c4281fa772bea4c193fffe77cfebf2f1"
    Content-Type: application/json
    ...

    ... data ...

=item 3

The API stores the C<ETag> value along with the data in this cache, for
given URL.

=item 4

Another request to get user data is made. This time, since the C<ETag>
is stored in the cache, it will be included in the request:

    GET http://172.17.0.2/api/v4/user
    If-None-Match: W/"c4281fa772bea4c193fffe77cfebf2f1"
    User-Agent: libwww-perl/6.26
    PRIVATE-TOKEN: ${TOKEN}

=item 5

If everything was OK, a response will be returned. Now it depends on whether
the entity was modified or not.

=over

=item

If the entity was not modified, the response will be much shorter:

    HTTP/1.1 304 Not Modified
    Connection: keep-alive
    ...
    ETag: W/"c4281fa772bea4c193fffe77cfebf2f1"
    ...

    <no Content-Type nor data>

=item

Otherwise the response will contain new data along with new C<ETag>

    HTTP/1.1 200 OK
    Connection: keep-alive
    ...
    ETag: W/"69f439c650df980276b0f01186acf43a"
    Content-Type: application/json
    ...

    ... new data ...

in which case the new data is cached, replacing the old entity.

=back

=back

=head2 Methods

=over

=item new()

    my $cache = GitLab::API::Cache->new();

Creates a new instance of the cache. Takes no arguments.

=item get()

    my ($tag, $data) = $cache->get($uri);

For a given C<$uri> returns cached ETag C<$tag> and parsed data C<$data>.
Returns C<undef> if there is no data for this uri.

=item set()

    $cache->set($uri, $tag, $data);

Stores a pair of C<$tag> and C<$data> for the given C<$uri>.
Replaces old value if exists.

=item flush()

    $cache->flush();
    $cache->flush($uri1, ...);

If no parameters are provided, deletes all stored entries. Otherwise deletes
only those entries specified as parameters.

=back

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> that loads all GitLab modules.

=back
