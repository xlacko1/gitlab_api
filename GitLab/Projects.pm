package GitLab::Projects;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    projects => {
        method      => "GET",
        path        => "/projects",
        query       => [qw(
            archived
            visibility
            order_by
            sort
            search
            simple
            owned
            membership
            starred
            statistics
            with_issues_enabled
            with_merge_requests_enabled
        )],
        paginated   => 1,
    },

    user_projects   => {
        method      => "GET",
        path        => "/users/<uid>/projects",
        query       => [qw(
            archived
            visibility
            order_by
            sort
            search
            simple
            owned
            membership
            starred
            statistics
            with_issues_enabled
            with_merge_requests_enabled
        )],
        paginated   => 1,
    },

    project_by_id => {
        method      => "GET",
        path        => "/projects/<id>",
        encode      => [qw(
            id
        )],
        query       => [qw(
            statistics
        )],
    },

    project_create => {
        method      => "POST",
        path        => "/projects",
        optional    => [qw(
            name
            path
            namespace_id
            description
            issues_enabled
            merge_requests_enabled
            jobs_enabled
            wiki_enabled
            snippets_enabled
            resolve_outdated_diff_discussions
            container_registry_enabled
            shared_runners_enabled
            visibility
            import_url
            public_jobs
            only_allow_merge_if_build_succeeds
            only_allow_merge_if_all_discussions_are_resolved
            lfs_enabled
            request_access_enabled
            tag_list
            avatar
            printing_merge_request_link_enabled
            ci_config_path
        )],
    },

    project_create_for_user => {
        method      => "POST",
        path        => "/projects/user/<uid>",
        required    => [qw(
            name
        )],
        optional    => [qw(
            path
            default_branch
            namespace_id
            description
            issues_enabled
            merge_requests_enabled
            jobs_enabled
            wiki_enabled
            snippets_enabled
            resolve_outdated_diff_discussions
            container_registry_enabled
            shared_runners_enabled
            visibility
            import_url
            public_jobs
            only_allow_merge_if_build_succeeds
            only_allow_merge_if_all_discussions_are_resolved
            lfs_enabled
            request_access_enabled
            tag_list
            avatar
            printing_merge_request_link_enabled
            ci_config_path
        )],
    },

    project_edit => {
        method      => "PUT",
        path        => "/projects/<id>",
#       required    => [qw(
#       )],
        optional    => [qw(
            name
            path
            default_branch
            namespace_id
            description
            issues_enabled
            merge_requests_enabled
            jobs_enabled
            wiki_enabled
            snippets_enabled
            resolve_outdated_diff_discussions
            container_registry_enabled
            shared_runners_enabled
            visibility
            import_url
            public_jobs
            only_allow_merge_if_build_succeeds
            only_allow_merge_if_all_discussions_are_resolved
            lfs_enabled
            request_access_enabled
            tag_list
            avatar
            printing_merge_request_link_enabled
            ci_config_path
        )],
    },

    project_fork => {
        method      => "POST",
        path        => "/projects/<id>/fork",
        encode      => [qw(
            id
            namespace
            namespace_path
            path
        )],
        optional    => [qw(
            name
            namespace_id
            namespace_path
            namespace
            path
            description
            visibility
        )],
    },

    project_list_forks => {
        method      => "GET",
        path        => "/projects/<id>/forks",
        encode      => [qw(
            id
        )],
        query       => [qw(
            archived
            visibility
            order_by
            sort
            search
            simple
            owned
            membership
            starred
            statistics
            with_issues_enabled
            with_merge_requests_enabled
        )],
        paginated   => 1,
    },

    project_star => {
        method      => "POST",
        path        => "/projects/<id>/star",
        encode      => [qw(
            id
        )],
    },

    project_unstar => {
        method      => "DELETE",
        path        => "/projects/<id>/unstar",
        encode      => [qw(
            id
        )],
    },

    project_archive => {
        method      => "POST",
        path        => "/projects/<id>/archive",
        encode      => [qw(
            id
        )],
    },

    project_unarchive => {
        method      => "DELETE",
        path        => "/projects/<id>/unarchive",
        encode      => [qw(
            id
        )],
    },

    project_delete => {
        method      => "DELETE",
        path        => "/projects/<id>",
        encode      => [qw(
            id
        )],
    },

#   DO NOT USE, not sure how this is supposed to work
#   project_upload_file => {
#       method      => "POST",
#       path        => "/projects/<id>/uploads",
#       encode      => [ qw(id) ],
#       required    => [ qw(file) ],
#   },

    project_share => {
        method      => "POST",
        path        => "/projects/<id>/share",
        required    => [qw(
            group_id
            group_access
        )],
        optional    => [qw(
            expires_at
        )],
        encode      => [qw(
            id
        )],
    },

    project_unshare => {
        method      => "DELETE",
        path        => "/projects/<id>/share/<group_id>",

    },

    project_hooks => {
        method      => "GET",
        path        => "/projects/<id>/hooks",
        encode      => [qw(
            id
        )],
        paginated   => 1,
    },

    project_get_hook => {
        method      => "GET",
        path        => "/projects/<id>/hooks/<hook_id>",
        encode      => [qw(
            id
        )],
    },

    project_add_hook => {
        method      => "POST",
        path        => "/projects/<id>/hooks",
        encode      => [qw(
            id
        )],
        required    => [qw(
            url
        )],
        optional    => [qw(
            push_events
            issues_events
            merge_requests_events
            tag_push_events
            note_events
            job_events
            pipeline_events
            wiki_page_events
            enable_ssl_verification
            token
        )],
    },

    project_edit_hook => {
        method      => "PUT",
        path        => "/projects/<id>/hooks/<hook_id>",
        encode      => [qw(
            id
        )],
        optional    => [qw(
            push_events
            issues_events
            merge_requests_events
            tag_push_events
            note_events
            job_events
            pipeline_events
            wiki_page_events
            enable_ssl_verification
            token
        )],
    },

    project_delete_hook => {
        method      => "DELETE",
        path        => "/projects/<id>/hooks/<hook_id>",
        encode      => [qw(
            id
        )],
    },

    project_create_forked_relationship => {
        method      => "POST",
        path        => "/projects/<id>/fork/<forked_from_id>",
        encode      => [qw(
            id
            forked_from_id
        )],
    },

    project_delete_forked_relationship => {
        method      => "DELETE",
        path        => "/projects/<id>/fork",
        encode      => [qw(
            id
        )],
    },

    project_start_housekeeping => {
        method      => "POST",
        path        => "/projects/<id>/housekeeping",
        encode      => [qw(
            id
        )],
    },

    project_transfer => {
        method      => 'PUT',
        path        => '/projects/<id>/transfer',
        required    => [qw(
            namespace
        )],
        encode      => [qw(
            namespace
        )],
    },

    project_export => {
        method      => 'POST',
        path        => '/projects/<id>/export',
        optional    => [qw(
            description
        )],
    },

    project_export_status => {
        method      => 'GET',
        path        => '/projects/<id>/export',
    },

    project_export_download => {
        method      => 'GET',
        path        => '/projects/<id>/export/download',
        download    => 1,
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Projects - implements projects API calls

See L<GitLab API -- Projects|http://doc.gitlab.com/ce/api/projects.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
