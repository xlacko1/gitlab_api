package GitLab::API;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use Carp            qw();
use Data::Dumper;
use HTTP::Headers;
use HTTP::Status    qw(:constants);
use JSON;
use LWP::UserAgent;
use Log::Any        qw($log);
use Scalar::Util    qw(blessed);
use URI;
use URI::Escape;
use URI::QueryParam;

use GitLab::API::Cache;
use GitLab::API::Guard;
use GitLab::API::Iterator;

use parent "Exporter";

our $VERSION = 10.2.4;

my %defaults = (
    per_page        => 50,
    die_on_error    =>  1,
    keep_alive      =>  1,
);

#===============================================================================
#   Constructor
#===============================================================================

sub carp(@)  { $log->warn(@_);  Carp::carp  @_; }
sub croak(@) { $log->fatal(@_); Carp::croak @_; }

sub new {
    my ($class, %args) = @_;

    croak "either URL or Host parameter is required, but not both"
        unless (defined $args{URL} xor defined $args{Host});

    if (defined $args{Host}) {
        my $s = ($args{Secure} // 1) ? "https" : "http";
        my $v = $args{Version} // 4;
        $args{URL} = "$s://$args{Host}/api/v$v";
    }

    my ($req_version) = ($args{URL} =~ m!api/v(\d+)!);
    if (!defined $req_version) {
        carp "Cannot determine requested version of API";
    } elsif ($req_version < 4) {
        carp "API version $req_version is deprecated";
    } elsif ($req_version > 4) {
        croak "API version $req_version is not supported";
    }

    $log->debug("initializing GitLab::API for \"$args{URL}\"");

    my $self = {
        api_url => $args{URL},
        json    => JSON->new(),
        unwrap  => 0,
        deadly  => $args{DieOnError} // $defaults{die_on_error},
    };

    # allow direct string decoding
    $self->{json}->allow_nonref();

    # bless $self before calling other methods
    bless $self, $class;

    if (!defined $args{AuthToken}) {
        croak "Either AuthToken or Login, Email and Password arguments are required"
            unless (defined $args{Password} && (defined $args{Login} || defined $args{Email}));

        # obtain AuthToken
        $self->login(\%args);
        croak "Invalid credentials" unless defined $self->{token};
    } else {
        $self->{token} = $args{AuthToken};
    }

    $self->{http} = LWP::UserAgent->new(
        default_headers => HTTP::Headers->new(
            "Private-Token" => $self->{token},
        ),
        keep_alive => $defaults{keep_alive},
    );

    $log->debug("testing connection");
    my ($user, $response) = $self->whoami(-response => 1);

    croak "GitLab authentication failed: ", $response->message
        unless $response->is_success;

    $self->{session} = $user    unless defined $self->{session};

    my $version = $self->version(-immortal => 1);

    my $vn = defined $version
        ? "$version->{version} ($version->{revision})"
        : "unknown (probably before 8.13)";

    $log->debug("setup complete, logged in as '$user->{username}' ($user->{name}), GitLab version is $vn");

    # setup ETag cache
    if ($args{Cache}) {
        $version = $version // { version => "0.0.0" };
        my ($v_major, $v_minor, $v_release) =
            ($version->{version} =~ m/^(\d+)\.(\d+)\.(\d+)/);

        $log->warn("ETag caching might not work on GitLab prior 9.0.0")
            if $v_major < 9;

        $self->{cache} = GitLab::API::Cache->new;
    }

    return $self;
}

sub sudo {
    my ($self, $who, $code) = @_;

    carp "Using sudo() without administrator privileges"
        if defined $who && !$self->is_admin;

    $log->debug(defined $who ? "impersonating '$who'" : "stopped impersonation");
    $self->http->default_header(Sudo => $who);
    $self->{impersonate} = $who;

    return if !defined $who || (!defined wantarray && !defined $code);

    my $guard = GitLab::API::Guard->new(sub { $self->sudo(undef); });
    return $code->() if defined $code;
    return $guard;
}

sub http     { return shift->{http};     }
sub json     { return shift->{json};     }
sub cache    { return shift->{cache};    }
sub response { return shift->{response}; }

sub die_on_error {
    my ($self, $val) = @_;
    return $self->{deadly} unless defined $val;

    $self->{deadly} = $val;
}

sub is_admin {
    return $_[0]->{session}->{is_admin};
}

#===============================================================================
#   Internals
#===============================================================================

sub login {
    my ($self, $args) = @_;
    my $agent = LWP::UserAgent->new;

    my $content = [ password => $args->{Password} ];

    if (defined $args->{Login}) {
        $log->debug("logging in with username '$args->{Login}'");
        push @$content, login => $args->{Login} ;
    } else {
        $log->debug("logging in with e-mail '$args->{Email}'");
        push @$content, email => $args->{Email} ;
    }

    my $response = $agent->post($self->{api_url} . "/session", $content);

    if ($response->is_success) {
        $log->debug("login successful");
        $self->{session}    = $self->json->decode($response->decoded_content);
        $self->{token}      = $self->{session}->{private_token};
    } else {
        $log->fatal("login failed: " . $response->status_line);
    }
}

sub create_uri {
    my ($self, $tmpl, $args) = @_;

    # creare URI path
    my $path     = substr $tmpl->{path}, 0;
    my %patharg  = map { $_ => 1 } ($path =~ m!<([^<]*)>!g);
    my %encode   = map { $_ => 1 } @{$tmpl->{encode}};

    foreach my $arg (keys %patharg) {
        croak "Argument '$arg' is required for '$tmpl->{name}'"
            unless defined $args->{$arg};

        my $val = URI::Escape::uri_escape($args->{$arg});

        $path =~ s!<$arg>!$val!g;
        delete $args->{$arg};
    }

    my $uri = URI->new($self->{api_url} . $path);

    # add query parameters
    foreach my $param (@{$tmpl->{query} // []}) {
        my $type = '';
        if ($param =~ s/\[\]$//) {
            $type = 'ARRAY';
        } elsif ($param =~ s/\{\}//) {
            $type = 'HASH';
        }

        if (exists $args->{$param}) {
            if (ref $args->{$param} ne $type) {
                croak "Argument '$param' is expected to be of type '$type',
                        but '" . ref($args->{$param}) . "' is provided";
            }

            if (ref $args->{$param} eq "ARRAY") {
                foreach my $val (@{ $args->{$param} }) {
                    $uri->query_param_append("$param\[\]", $val);
                }
            } elsif (ref $args->{$param} eq "HASH") {
                while (my ($key, $val) = each %{$args->{$param}}) {
                    $uri->query_param_append("$param\[$key\]", $val);
                }
            } else {
                $uri->query_param_append($param => $args->{$param});
            }

            delete $args->{$param};
        }
    }

    return $uri;
}

sub _param_exists {
    my ($self, $tmpl, $param) = @_;

    return grep { $_ eq $param }
        @{ $tmpl->{optional} // [] },
        @{ $tmpl->{required} // [] };
}

sub process_post_args {
    my ($self, $tmpl, $args) = @_;

    my @result;
    foreach my $param (keys %$args) {
        if (ref $args->{$param} eq 'ARRAY') {
            if (!$self->_param_exists($tmpl, $param . '[]')) {
                croak "$param: Is an array but $param\[\] is not allowed for this request";
            }

            foreach my $value (@{ $args->{$param} }) {
                push @result, "$param\[\]", $value;
            }
        } elsif (ref $args->{$param} eq 'HASH') {
            if (!$self->_param_exists($tmpl, $param . '{}')) {
                croak "$param: Is a hash but $param\{\} is not allowed for this request";
            }

            foreach my $key (sort keys %{ $args->{$param} }) {
                push @result, "$param\[$key\]", $args->{$param}->{$key};
            }
        } else {
            if (!$self->_param_exists($tmpl, $param)) {
                croak "$param: Is a scalar but $param is not allowed for this request";
            }

            push @result, $param, $args->{$param};
        }
    }

    return @result;
}

sub clean_data {
    my ($self, $response, $tmpl) = @_;

    # if the request failed on clinet site, LWP sets "Client-Warning: Internal response" header
    if (!$response->is_success && ($response->header("Client-Warning") // "") eq "Internal response") {
        croak $response->message;
    }

    # no data should be present if we got 204 No Content
    return $response
        if $response->code == HTTP_NO_CONTENT;

    # if the template specifies that we should have gotten a download file,
    # return the raw data instead
    if ($tmpl->{download}) {
        return ($response, $response->decoded_content);
    }

    # otherwise we should have gotten application/json response
    croak "GitLab sent '", $response->header("Content-Type"), "' instead of 'application/json'"
        if $response->header("Content-Type") !~ m!^application/json!;

    # don't process data on error
    return ($response, undef)
        unless $response->is_success;

    my $data = $response->decoded_content;
    utf8::decode($data);
    return ($response, $self->json->decode($data));
}

#--  Request wrappers  ---------------------------------------------------------

sub exec_request {
    my ($self, $tmpl, $args) = @_;

    my @rtkeys = grep { $_ =~ m/^-/ } (keys %$args);
    my $rtargs = { %{$args}{@rtkeys} };
    delete @{$args}{@rtkeys};

    if ($log->is_debug) {
        my @bits;

        for my $key (keys %$args) {
            my $value = $key eq "password"      ? "<redacted>"
                      : !defined $args->{$key}  ? "<undef>"
                      : "\"$args->{$key}\"";

            push @bits, "$key => $value";
        }

        push @bits, "[$_ => $rtargs->{$_}]" foreach @rtkeys;
        $log->debug("called GitLab::API::$tmpl->{name}(" . (join ", ", @bits) . ")");
    }

    my $uri = $self->create_uri($tmpl, $args);

    # validate required arguments
    my @missing = grep { !defined $args->{$_} } @{$tmpl->{required}};
    croak "Missing arguments [" . join(", ", @missing) . "] for '$tmpl->{name}'" if scalar @missing;

    # validate optional arguments
    my %allargs = map { ($_ =~ s/(\[\]|\{\})$//gr) => 1 }
            (@{$tmpl->{required}}, @{$tmpl->{optional}});

    my @extra   = grep { !defined $allargs{$_} }
            keys %$args;

    carp "Extra arguments [" . join(", ", @extra) . "] for '$tmpl->{name}'" if scalar @extra;

    my @postdata = $self->process_post_args($tmpl, $args);
    my $data = [];
    my $response;

    croak "Cannot request -iterator if -page is specified as well"
        if $rtargs->{-iterator} && defined $rtargs->{-page};

    croak "Requested iterator for a non-paginated method"
        if $rtargs->{-iterator} && !$tmpl->{paginated};

    $log->debug("$tmpl->{method} $uri");

    if ($tmpl->{method} eq "GET") {
        my $paginated = $tmpl->{paginated} && !defined $rtargs->{-page};

        if (defined $rtargs->{-page}) {
            $uri->query_param(page      => $rtargs->{-page});
            $uri->query_param(per_page  => $rtargs->{-per_page} // $defaults{per_page});
        }

        if ($paginated) {
            $log->debug("using iterator to obtain the result");

            my $iterator = GitLab::API::Iterator->new($self, $uri,
                per_page => $rtargs->{-per_page} // $defaults{per_page},
                template => $tmpl);

            return $rtargs->{-iterator} ? $iterator : $iterator->all;
        }

        my %headers;
        my ($tag, $cached);

        # try using cache if defined
        if (defined $self->cache) {
            ($tag, $cached) = $self->cache->get($uri);
            $headers{"If-None-Match"} = $tag if (defined $tag);
        }

        my $httpraw = $self->http->get($uri, %headers);
        if (defined $self->cache && $httpraw->code == HTTP_NOT_MODIFIED) {
            ($response, $data) = ($httpraw, $cached);
        } else {
            $self->cache->flush($uri) if defined $self->cache;
            ($response, $data) = $self->clean_data($httpraw, $tmpl);
        }
    } elsif ($tmpl->{method} eq "POST") {
        ($response, $data) = $self->clean_data($self->http->post($uri, \@postdata), $tmpl);
    } elsif ($tmpl->{method} eq "PUT") {
        ($response, $data) = $self->clean_data($self->http->put($uri, \@postdata), $tmpl);
    } elsif ($tmpl->{method} eq "DELETE") {
        ($response, $data) = $self->clean_data($self->http->delete($uri), $tmpl);
    } else {
        croak "Unsupported method '$tmpl->{method}'";
    }

    # dump HTTP request and response if using trace
    if ($log->is_trace) {
        $log->trace("----  HTTP REQUEST  ".("-" x 25)."\n".$response->request->as_string);
        $log->trace("----  HTTP RESPONSE ".("-" x 25)."\n".$response->as_string);
        $log->trace("--------------------".("-" x 25));
    }

    $log->debug("status: " . $response->status_line);
    $data = undef if !$response->is_success && $response->code != HTTP_NOT_MODIFIED;
    $self->{response} = $response;

    if (defined $self->cache && $tmpl->{method} eq "GET") {
        my $tag = $response->header("ETag");
        $self->cache->set($uri, $tag, $data)
            if defined $tag;
    }

    # is it OK to die?
    croak "$tmpl->{name} failed: " . $response->status_line
        if $self->die_on_error
            && !$rtargs->{-immortal}
            && !($response->is_success || $response->code == HTTP_NOT_MODIFIED);

    return $rtargs->{-response} ? ($data, $response) : $data;
}

#===============================================================================
#   Package methods
#===============================================================================

# methods registered with the API
my %methods;

sub methods {
    return [ keys %methods ];
}

sub register {
    my ($class, @templates) = @_;

    for my $template (@templates) {
        croak "Missing 'name' attribute in a template"
            if !defined $template->{name};

        croak "Method '$template->{name}' is already registered"
            if  exists $methods{$template->{name}};

        $log->debug("registering method '$template->{name}'");
        $methods{$template->{name}} = $template;
    }
}

# Autoload methods on demand
sub AUTOLOAD {
    my ($self, @ARGS) = @_;
    my $method = our $AUTOLOAD;
    $method =~ s/.*:://g;

    croak "Not a GitLab::API instance"
        if !blessed($self) || !$self->isa(__PACKAGE__);

    croak "Unknown method '$AUTOLOAD' called"
        if !defined $methods{$method};

    $log->debug("autoloading method '$method'");

    no strict "refs";
    *{__PACKAGE__ . "::" . $method} = sub {
        my ($self, %args) = @_;
        return $self->exec_request($methods{$method}, \%args);
    };

    $self->$method(@ARGS);
}

# The following code registers "whoami" method needed by the constructor.
BEGIN {
    __PACKAGE__->register({
        name        => "whoami",
        method      => "GET",
        path        => "/user",
    }, {
        name        => "version",
        method      => "GET",
        path        => "/version",
    });
}

sub DESTROY {
    # nothing, exists only to please AUTOLOAD
}

1;

#===============================================================================
#   DOCUMENTATION
#===============================================================================

__END__

=head1 NAME

GitLab::API - lightweight GitLab API client.
Also implements B<Session> API to obtain authentication token.
Other modules are needed for additional methods.

See L<GitLab API|http://doc.gitlab.com/ce/api/> for details.

=head1 SYNOPSIS

    use GitLab::API;
    use GitLab::Users;
    use GitLab::Groups;
    use GitLab::Projects;

    my $gitlab = GitLab::API->new(
        AuthToken   => $token,
        Host        => $host,
    );

    # simple result
    my $groups = $gitlab->groups(search => "awesome-group");

    die("Failed to get groups") unless defined $groups;

    # result with response
    my ($users, $response) = $gitlab->users(-response => 1);

    die("Request failed: " . $response->message) unless $response->is_success;

    # iterator for paginated responses
    my $projects = $gitlab->projects(-iterator => 1);

    while (my $project = $projects->next) {
        # ...
    }

=head1 DESCRIPTION

=over

=item new()

    $gitlab = GitLab::API->new(arg => value, ...);

Creates a new instance of L<GitLab::API>. Takes a hash of arguments:

=over

=item * Host specification

Either C<URL> or C<Host> is required, but not both.

=over

=item C<URL>

Fully specifies the API to connect to. Usually in format
L<http(s)://gitlab.domain.com/api/v4>. Must not be combined with C<Host>.

=item C<Host>

Hostname of GitLab. Must not be combined with C<URL>.

=item C<Version>

Version of GitLab API to connect to. The default is C<4> and you should
not change this unless you really need other versions.
This options is ignored if C<URL> is used.

=item C<Secure>

Whether or not use HTTPS. The default is C<1>.
This option is ignored if C<URL> is used.

=back

=item * Login information

There are three ways to log in to GitLab, using a combination of parameters:

=over

=item C<AuthToken>

Use GitLab Personal Token to access API. Does not need to use the Session
API, hence avoids one step when setting up L<GitLab::API>.

=item C<Login>, C<Password>

This combination will use Session API to obtain token. Then it will
continue as C<AuthToken> was specified.

=item C<Email>, C<Password>

An alternative to C<Login> and C<Password>.

=back

=item * Miscellaneous

=over

=item C<DieOnError>

API will call L<Carp/croak> if any request fails. This general option
can be overriden when needed, by setting C<< -immortal => 1 >> to request.

=item C<Cache>

API will use L<GitLab::API::Cache> to store data along with their C<ETags>
to speed up repetitive requests. This may, however, consume more memory.
You may manually clear the cache by calling

    $gitlab->cache->flush;

The ETags are fully supported in GitLab since version 9.0.0. If L<GitLab::API>
detects that GitLab is of earlier version, a warning will be logged.

=back

=back

=item sudo()

    $gitlab->sudo($username);
    $gitlab->sudo();

Similar to L<sudo(8)|sudo>, changes identity to the user with the C<$username>.
If the C<$username> argument is not defined, changes the identity back to the original user.

I<This feature is available only to the admins.>
You can test for that with the L</is_admin> method like this:

    if ($gitlab->is_admin) {
        # Booyah!
    }

=item whoami()

Returns the user whose authentication key is used for the request (or impersonated user if L</sudo> is in effect).
See L</"REQUEST METHODS"> below.

=item version()

Returns the current GitLab version and revision.

=item is_admin()

    $gitlab->is_admin()

Returns true if the logged in user has administrator permissions.
The result is not affected by L</sudo()>.

=item die_on_error()

    my $value = $gitlab->die_on_error();
    $gitlab->die_on_error($value);

When called without an argument, returns the current value of the setting.
Otherwise sets the first argument as the value.

If enabled, any call that returns an unsuccessful (!= 2**) HTTP code will call C<Carp/croak> with the status code.
If disabled, unsuccessful calls will simply return C<undef>.

The setting can be 'overriden' temporarily with the C<< -immortal => 1 >> option passed to the method.
See L</exec_request> below.

=back

=head2 Internal Methods

The following methods are intended to be called from additional methods implemented by C<GitLab::*> modules.
B<It is not recommended to use these methods directly.>

=over

=item http()

Returns the HTTP User Agent.

=item json()

Returns the JSON decoder.

=item response()

Returns the HTTP response of the last method.

=item login()

    $gitlab->login(arg => value, ...);

With provided login information (C<Email>, C<Login>, C<Password>), this method attempts to obtain user's private token.
Under the hood it simply calls the L<session|http://docs.gitlab.com/ce/api/session.html> API method.

=item create_uri()

    $uri = $gitlab->create_uri($template, $arguments);

Constructs an L<URI> instance for the template using given arguments.

=item decode_data()

    $data = $gitlab->decode_data($response);

Decodes the JSON content from the C<$response> and converts it to Perl data.

=item exec_request()

    $data = $gitlab->exec_request($template, $arguments);

    # if arguments contain {-response => 1}
    ($data, $response) = $gitlab->exec_request($template, $arguments);

Sends a request constructed from the given C<$template> and C<$arguments> hashref.
Arguments that start with C<-> (minus) are reserved to modify the client's behaviour and are B<never> forwarded to GitLab.

Returns data received from GitLab API, either as hashref or arrayref (this depends on the request) or C<undef> if the request failed (and C<die_on_error> is disabled).
Additionally it saves the last L<HTTP::Response> object as the C<response> attribute of C<$self>.

The following arguments can be used to modify L</exec_request>'s behaviour:

=over

=item C<< -response => [0|1] >>

Return both data and the L<HTTP::Response> object as a list of two elements if set to true value.

    my ($data, $response) = $gitlab->$method(..., -response => 1);

=item C<< -immortal => [0|1] >>

When enabled, calls will not not carp on error even when C<die_on_error> is enabled.
Recommended with C<-response> option so that the status code can be inspected directly.

    my ($data, $response) = $gitlab->$method(..., -response => 1, -immortal => 1);
    if (!$response->is_success) {
        # handle the error
    }

=item C<< -page => N >>

When used with paginated requests, returns only entries from the given page.
The number of entities in each page is by default I<50> except (of course) the last one.
It is possible to change the default page size with the C<<-per_page>> option.
The API returns an empty response for invalid page numbers.

The method I<croaks> if used with a template without C<< $tmpl->{paginated} >> attribute or if combined with C<< -iterator >>.

=item C<< -per_page => N >>

Change the page size for C<<-page>> or C<<-iterator>> option.
The maximum allowed value by GitLab is currently C<100>.

=item C<< -iterator >>

When non-zero, returns an instance of L<GitLab::API::Iterator> that will sequentially request pages when needed.

The method I<croaks> if combined with C<<-page>> or used for non-paginated request.

=back

=back

=head1 Package methods

=over

=item methods()

    $methods = GitLab::API::methods();

Returns an arrayref of method names registered with the API.
This method cannot be exported, use

    GitLab::API::methods()

to obtain the result.

=back

=head1 REQUEST METHODS

Request methods are methods that create requests and call GitLab API.
This package provides an easy data-driven way to create such methods with
request templates (see L</"REQUEST TEMPLATES"> section and L</register>
method below).

Unless specified otherwise, these methods take a hash of options, e.g.

    $gitlab->$method(key => value, ...);

Generally, keys starting with the C<-> (minus) characters are intended to modify the underlying C</exec_request>'s behaviour and are not forwarded to GitLab.

This module contains only L</whoami> and L</version> API methods, other methods are provided
by additional C<GitLab::*> modules.

=head1 REQUEST TEMPLATES

This section contains instruction on how to implement API request methods.
See L<GitLab::Users> and L<GitLab::Groups> for examples.

The requests are made by calling the L</exec_request> method with the following parameters:

    $tmpl       request template
    $args       hashref of request arguments

The I<request template> is a hashref with the following keys:

    method      [required] GET, POST, PUT or DELETE
    path        [required] URL relative to the URL parameter given to constructor
    paginated   if set to true, the request will be repeated and results
                concatented until all items are obtained
    query       arrayref of paramters that should be passed as query string in the URL
    required    arrayref of required arguments
    optional    optional arguments
    ret         hashref of HTTP codes whose values will be used as status messages
    encode      arrayref of path components that should be URI-encoded

The C<path> value can contain placeholders in the format C<< <name> >>, e.g. C<< /groups/<gid>/projects/<projid> >> is a path that contains placeholders C<gid> and C<projid>.
When processing the template, these placeholders will automatically become I<required arguments>.

Arguments specified in the C<query> are I<not> required by default, hence, if missing, they will not be included in the final URL.

Additional arguments (those not mentioned in C<query>) will be passed as the request content, that is, C<arg=value&arg=value...>, since the request's C<Content-Type> is C<application/x-www-form-urlencoded> by default.

Optional arguments can be specified by the C<optional> key.
The value of this key is an arrayref with strings.

As the name suggests, I<required arguments> must be provided, otherwise the L</exec_request> will croak.

All values for C<path> placeholders are encoded before replacement.
However, if the value can contain reserved URI characters (e.g. C</>), the encoding must be forced.
This can be done by specifying this placeholder in the C<encode> arrayref.

=head2 Examples

=head3 Simple

Consider the following template hashref:

    my $user_by_id = {
        name        => "user_by_id",
        method      => "GET",
        path        => "/users/<uid>",
        ret         => { 404 => "User not found" },
    };

Called as C<< $gitlab->exec_request($user_by_id, { uid => 10 }) >>, the method will replace C<< <uid> >> placeholder with C<10> and then it will call

    GET .../api/v3/users/10

with empty content.
The C<uid> argument is required; the method will croak if it is not present.

If the given user is not found, the method will return status C<404> with reason C<User not found>.
For other status codes it will return the reason provided either by GitLab or underlying HTTP client.

=head3 Required arguments

    my $user_add_ssh_key = {
        name        => "user_add_ssh_key",
        method      => "POST",
        path        => "/users/<uid>/keys",
        required    => [ qw!title key! ],
    };

This request requires arguments C<title>, C<key> and C<uid> (since it appears in the C<path>).
The latter argument will be substituted in the path, the former two will be passed in the body of the request.

=head3 Encoding

    my $project_by_id = {
        method      => "GET",
        path        => "/projects/<id>",
        encode      => [ qw(id) ],
    };

From API documentation, this operation returns a project with the given C<id>, which can be a numerical value or a string C<"NAMESPACE/PROJECT_NAME">.

Without the C<encode> parameter in the template, the following request

    $gitlab->project_by_id("user/project");

would result in the path C<gitlab_url/projects/user/project> which is invalid.
The slash, as a reserved character, needs to be encoded using C<encode_required> parameter for L<URI::Encode>.
This option is enabled by specifying C<id> in the C<encode> parameter in the template.
Final URL in this case would be C<gitlab_url/projects/user%2Fproject>.

=head3 Pagination and optional arguments

    my $groups  = {
        name        => "groups",
        method      => "GET",
        path        => "/groups",
        query       => [ qw!search! ],
        paginated   => 1,
    };

When called, the request will be repeated with increasing C<page> query value until all pages are returned.
These responses will be concatenated to a single one.
The method will croak if any of the returned pages is not an array.

In addition, the request I<can> have (but does not require) an argument C<search> that will be passed in the query string (C<...?search=$search>).
In order to make this argument required, the template would have to contain the following key:

        required    => [ qw!search! ],

as well.

=head2 Registering methods

=over

=item register()

    GitLab::API->register($template, ...);

Instead of modifying L<GitLab::API> source to add a new method, or writing wrapper subs like this:

    sub my_method {
        my ($api, $args) = @_;
        return $api->exec_request($my_method_template, $args);
    }

for each request, the C<register> method creates a convenience wrapper method in this package.
The above example can be written as the following:

    GitLab::API->register($my_method_template);

    # ...

    $api->my_method($args);

The method name is the same as C<< $my_method_template->{name} >> attribute.

The call will fail if a template does not containt the C<name> attribute or a method with the same name already exists.

=back

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab::API::v3>

Complete GitLab API v3 implementation with CLI support.

=item L<GitLab>

    use GitLab;

Wrapper around L<GitLab::API> that loads all GitLab modules.

=back
