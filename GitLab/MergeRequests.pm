package GitLab::MergeRequests;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any qw($log);

our $VERSION = v13.0.0;

my $requests = {
    merge_requests => {
        method => 'GET',
        path => '/merge_requests',
        optional => [qw(
            approved_by_ids
            approver_ids
            assignee_id
            author_id
            author_username
            created_after
            created_before
            deployed_after
            deployed_before
            environment
            in
            labels
            milestone
            my_reaction_emoji
            not
            order_by
            reviewer_id
            reviewer_username
            scope
            search
            sort
            source_branch
            state
            target_branch
            updated_after
            updated_before
            view
            wip
            with_labels_details
            with_merge_status_recheck
        )],
        paginated => 1,
    },

    project_merge_requests => {
        method => 'GET',
        path => '/projects/<id>/merge_requests',
        optional => [qw(
            approved_by_ids
            approver_ids
            assignee_id
            author_id
            author_username
            created_after
            created_before
            iids
            labels
            milestone
            my_reaction_emoji
            not
            order_by
            reviewer_id
            reviewer_username
            scope
            search
            sort
            source_branch
            state
            target_branch
            updated_after
            updated_before
            view
            wip
            with_labels_details
            with_merge_status_recheck
        )],
        paginated => 1,
    },

    group_merge_requests => {
        method => 'GET',
        path => '/groups/<gid>/merge_requests',
        optional => [qw(
            approved_by_ids
            approver_ids
            assignee_id
            author_id
            author_username
            created_after
            created_before
            iids
            labels
            milestone
            my_reaction_emoji
            not
            order_by
            reviewer_id
            reviewer_username
            scope
            search
            sort
            source_branch
            state
            target_branch
            updated_after
            updated_before
            view
            wip
            with_labels_details
            with_merge_status_recheck
        )],
        paginated => 1,
    },

    project_merge_request_get => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>',
        optional => [qw(
            render_html
            include_diverged_commits_count
            include_rebase_in_progress
        )],
    },

    project_merge_request_get_participants => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/participants',
        paginated => 1,
    },

    project_merge_request_get_commits => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/commits',
        paginated => 1,
    },

    project_merge_request_get_changes => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/changes',

        optional => [qw(
            access_raw_diffs
        )],
        paginated => 1,
    },

    project_merge_request_get_pipelines => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/pipelines',
        paginated => 1,
    },

    project_merge_request_create_pipeline => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/pipelines',
    },

    project_merge_request_create => {
        method => 'POST',
        path => '/projects/<id>/merge_requests',
        encode => [qw(
            id
            target_project_id
        )],
        required => [qw(
            source_branch
            target_branch
            title
        )],
        optional => [qw(
            assignee_id
            assignee_ids[]
            reviewer_ids[]
            description
            target_project_id
            labels
            milestone_id
            remove_source_branch
            allow_collaboration
            allow_maintainer_to_push
            squash
        )],
    },

    project_merge_request_update => {
        method => 'PUT',
        path => '/projects/<id>/merge_requests/<mr_id>',
        optional => [qw(
            target_branch
            title
            assignee_id
            assignee_ids
            reviewer_ids
            milestone_id
            labels
            add_labels
            remove_labels
            description
            state_event
            remove_source_branch
            squash
            discussion_locked
            allow_collaboration
            allow_maintainer_to_push
        )],
    },

    project_merge_request_delete => {
        method => 'DELETE',
        path => '/projects/<id>/merge_requests/<mr_id>',
    },

    project_merge_request_accept => {
        method => 'PUT',
        path => 'PUT /projects/<id>/merge_requests/<mr_id>/merge',
        optional => [qw(
            merge_commit_message
            squash_commit_message
            squash
            should_remove_source_branch
            merge_when_pipeline_succeeds
            sha
        )],
    },

    project_merge_request_accept_default_ref => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/merge_ref',
    },

    project_merge_request_cancel_when_pipeline_succeeds => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/cancel_merge_when_pipeline_succeeds',
    },

    project_merge_request_rebase => {
        method => 'PUT',
        path => 'PUT /projects/<id>/merge_requests/<mr_id>/rebase',
        optional => [qw(
            skip_ci
        )],
    },

    project_merge_request_get_issues => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/closes_issues',
        paginated => 1,
    },

    project_merge_request_subscribe => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/subscribe',
    },

    project_merge_request_unsubscribe => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/unsubscribe',
    },

    project_merge_request_todo => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/todo',
    },

    project_merge_request_diff_versions => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/versions',
        paginated => 1,
    },

    project_merge_request_diff_version_get => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/versions/<version_id>',
    },

    project_merge_request_set_time_estimate => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/time_estimate',
        required => [qw(
            duration
        )],
    },

    project_merge_request_reset_time_estimate => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/reset_time_estimate',
    },

    project_merge_request_add_spent_time => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/add_spent_time',
        required => [qw(
            duration
        )],
    },

    project_merge_request_reset_spent_time => {
        method => 'POST',
        path => '/projects/<id>/merge_requests/<mr_id>/reset_spent_time',
    },

    project_merge_request_time_stats => {
        method => 'GET',
        path => '/projects/<id>/merge_requests/<mr_id>/time_stats',
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::MergeRequests - implements Merge Requests API calls

See L<GitLab API -- Users|https://docs.gitlab.com/ce/api/merge_requests.html>
for details and response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back

