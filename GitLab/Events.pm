package GitLab::Events;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    self_events => {
        method      => "GET",
        path        => "/events",
        query       => [qw(
            action
            target_type
            before
            after
            sort
        )],
        paginated   => 1,
    },

    user_events => {
        method      => "GET",
        path        => "/user/<uid>/events",
        query       => [qw(
            action
            target_type
            before
            after
            sort
        )],
        paginated   => 1,
    },

    project_events => {
        method      => "GET",
        path        => "/projects/<id>/events",
        query       => [qw(
            action
            target_type
            before
            after
            sort
        )],
        paginated   => 1,
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Events - implements events API calls

See L<GitLab API -- Events|https://docs.gitlab.com/ce/api/events.html> for details and
response formats.

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
