package GitLab::Runners;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    user_runners => {
        method      => "GET",
        path        => "/runners",
        query       => [qw(
            scope
        )],
        paginated   => 1,
    },

    runners => {
        method      => "GET",
        path        => "/runners/all",
        query       => [qw(
            paused
            scope
            status
            type
        )],
        paginated   => 1,
    },

    runner_by_id => {
        method      => "GET",
        path        => "/runners/<rid>",
    },

    runner_update => {
        method      => "PUT",
        path        => "/runners/<rid>",
        optional    => [qw(
            description
            active
            tag_list
            run_untagged
            locked
            access_level
        )],
    },

    runner_delete => {
        method      => "DELETE",
        path        => "/runners/<rid>",
    },

    runner_jobs => {
        method      => "GET",
        path        => "/runners/<rid>/jobs",
        query       => [qw(
            status
            order_by
            sort
        )],
        paginated   => 1,
    },

    project_runners => {
        method      => "GET",
        path        => "/projects/<proj_id>/runners",
        paginated   => 1,
    },

    project_runner_enable => {
        method      => "POST",
        path        => "/projects<proj_id>/runners",
        required    => [qw(
            rid
        )],
        paginated   => 1,
    },

    project_runner_disable => {
        method      => "DELETE",
        path        => "/projects/<proj_id>/runners/<rid>",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Runners - implements runners API calls.

See L<GitLab API -- Projects|http://doc.gitlab.com/ce/api/runners.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
