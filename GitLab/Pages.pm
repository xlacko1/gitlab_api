package GitLab::Pages;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    pages => {
        method => 'GET',
        path => '/pages/domains',
        paginated => 1,
    },

    project_pages => {
        method => 'GET',
        path => '/projects/<id>/pages/domains',
        paginated => 1,
    },

    project_pages_domain_get => {
        method => 'GET',
        path => '/projects/<id>/pages/domains/<domain>',
    },

    project_pages_domain_create => {
        method => 'POST',
        path => '/projects/<id>/pages/domains',
        optional => [qw{
            auto_ssl_enabled
            certificate
            key
        }],
    },

    project_pages_domain_update => {
        method => 'PUT',
        path => '/projects/<id>/pages/domains/<domain>',
        optional => [qw{
            auto_ssl_enabled
            certificate
            key
        }],
    },

    project_pages_domain_delete => {
        method => 'DELETE',
        path => '/projects/<id>/pages/domains/<domain>',
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;
