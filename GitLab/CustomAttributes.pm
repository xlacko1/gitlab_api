package GitLab::CustomAttributes;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    # User attributes
    "<ENTITY>_custom_attributes" => {
        method      => "GET",
        path        => "/<ENTITY>s/<id>/custom_attributes",
    },

    "<ENTITY>_get_custom_attribute" => {
        method      => "GET",
        path        => "/<ENTITY>s/<id>/custom_attributes/<key>",
    },

    "<ENTITY>_set_custom_attribute" => {
        method      => "PUT",
        path        => "/<ENTITY>s/<id>/custom_attributes/<key>",
        required    => [qw(
            value
        )],
    },

    "<ENTITY>_delete_custom_attribute" => {
        method      => "DELETE",
        path        => "/<ENTITY>s/<id>/custom_attributes/<key>",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);

    my $emap = {
        user    => "uid",
        group   => "gid",
        project => "id",
    };

    while (my ($name, $tmpl) = each(%$requests)) {
        foreach my $entity (keys %$emap) {
            my $copy = { %$tmpl };

            $copy->{name} = $name
                unless exists $copy->{name};

            foreach my $v (values %$copy) {
                $v =~ s/<ENTITY>/$entity/g;
                $v =~ s/<id>/<$emap->{$entity}>/g;
            }

            GitLab::API->register($copy);
        }
    }
}

1;

__END__

=head1 NAME

GitLab::CustomAttributes - implements custom attributes API

See L<GitLab API -- Custom Attributes|https://docs.gitlab.com/ce/api/custom_attributes.html>
for details and response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
