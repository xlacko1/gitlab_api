package GitLab::Repositories;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v11.3.4;

my $requests = {
    repository_ls_tree => {
        method      => "GET",
        path        => "/projects/<pid>/repository/tree",
        optional    => [qw(
            path
            ref
            recursive
        )],
        paginated   => 1,
    },

    repository_get_blob => {
        method      => "GET",
        path        => "/projects/<pid>/repository/blobs/<sha>",
    },

    repository_get_blob_raw => {
        method      => "GET",
        path        => "/projects/<pid>/repository/blobs/<sha>/raw",
    },

    repository_get_archive => {
        method      => "GET",
        path        => "/projects/<pid>/repository/archive",
        optional    => [qw(
            sha
            format
        )],
    },

    repository_compare_refs => {
        method      => "GET",
        path        => "/projects/<pid>/repository/compare",
        required    => [qw(
            from
            to
        )],
        optional    => [qw(
            straight
        )],
    },

    repository_contributors => {
        method      => "GET",
        path        => "/projects/<pid>/repository/contributos",
        optional    => [qw(
            order_by
            sort
        )],
    },

    repository_merge_base => {
        method      => "GET",
        path        => "/projects/<pid>/repository/merge_base",
        required    => [qw(
            refs
        )],
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Repository - implements repositories API calls

See L<GitLab API -- Users|http://doc.gitlab.com/ce/api/repositories.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
