package GitLab::Users;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    users => {
        method      => "GET",
        path        => "/users",
        query       => [qw(
            username
            search
            blocked
            active
            extern_uid
            provider
            created_before
            created_after
            custom_attributes
            external
        )],
        paginated   => 1,
    },

    user_by_id => {
        method      => "GET",
        path        => "/users/<uid>",
    },

    user_create     => {
        method      => "POST",
        path        => "/users",
        required    => [qw(
            email
            username
            name
        )],
        optional    => [qw(
            password
            reset_password
            skype
            linkedin
            twitter
            website_url
            organization
            projects_limit
            extern_uid
            provider
            bio
            location
            admin
            can_create_group
            skip_confirmation
            external
            avatar
        )],
    },

    user_update     => {
        method      => "PUT",
        path        => "/users/<uid>",
        # update can cause Conflict, but API always returns 404
        ret         => {
            404 => "Not found or Conflict"
        },
        optional    => [qw(
            email
            username
            name
            password
            skype
            linkedin
            twitter
            website_url
            organization
            projects_limit
            extern_uid
            provider
            bio
            location
            admin
            can_create_group
            skip_reconfirmation
            external
            avatar
        )],
    },

    user_delete     => {
        method      => "DELETE",
        path        => "/users/<uid>",
    },

    # SSH keys
    self_ssh_keys => {
        method      => "GET",
        path        => "/user/keys",
        paginated   => 1,
    },

    user_ssh_keys => {
        method      => "GET",
        path        => "/users/<uid>/keys",
        paginated   => 1,
    },

    self_get_ssh_key => {
        method      => "GET",
        path        => "/user/keys/<keyid>",
    },

    self_add_ssh_key => {
        method      => "POST",
        path        => "/user/keys",
        required    => [qw(
            title
            key
        )],
    },

    user_add_ssh_key => {
        method      => "POST",
        path        => "/users/<uid>/keys",
        required    => [qw(
            title
            key
        )],
    },

    self_delete_ssh_key => {
        method      => "DELETE",
        path        => "/user/keys/<keyid>",
    },

    user_delete_ssh_key => {
        method      => "DELETE",
        path        => "/users/<uid>/keys/<keyid>",
    },

    # GPG keys
    self_gpg_keys => {
        method      => "GET",
        path        => "/user/gpg_keys",
        paginated   => 1,
    },

    user_gpg_keys   => {
        method      => "GET",
        path        => "/users/<uid>/gpg_keys",
        paginated   => 1,
    },

    self_get_gpg_key => {
        method      => "GET",
        path        => "/user/gpg_keys/<keyid>",
    },

    user_get_gpg_key => {
        method      => "GET",
        path        => "/users/<uid>/gpg_keys/<keyid>",
    },

    self_add_gpg_key => {
        method      => "POST",
        path        => "/user/gpg_keys",
        required    => [qw(
            key
        )],
    },

    user_add_gpg_key => {
        method      => "POST",
        path        => "/users/<uid>/gpg_keys",
        required    => [qw(
            key
        )],
    },

    self_delete_gpg_key => {
        method      => "DELETE",
        path        => "/user/gpg_keys/<keyid>",
    },

    user_delete_gpg_key => {
        method      => "DELETE",
        path        => "/users/<uid>/gpg_keys/<keyid>",
    },

    # E-mails
    self_get_emails => {
        method      => "GET",
        path        => "/user/emails",
        paginated   => 1,
    },

    user_get_emails => {
        method      => "GET",
        path        => "/users/<uid>/emails",
        paginated   => 1,
    },

    self_get_email => {
        method      => "GET",
        path        => "/user/emails/<eid>",
    },

    self_add_email => {
        method      => "POST",
        path        => "/user/emails",
        required    => [ qw!email! ],
    },

    user_add_email => {
        method      => "POST",
        path        => "/users/<uid>/emails",
        required    => [ qw!email! ],
        optional    => [qw(
            skip_confirmation
        )],
    },

    self_delete_email => {
        method      => "DELETE",
        path        => "/user/emails/<eid>",
    },

    user_delete_email => {
        method      => "DELETE",
        path        => "/users/<uid>/emails/<eid>",
    },

    # Tokens
    user_tokens => {
        method      => "GET",
        path        => "/users/<uid>/impersonation_tokens",
        query       => [qw(
            state
        )],
        paginated   => 1,
    },

    user_get_token => {
        method      => "GET",
        path        => "/users/<uid>/impersonation_tokens/<tokenid>",
    },

    user_add_token => {
        method      => "POST",
        path        => "/users/<uid>/impersonation_tokens",
        required    => [qw(
            name
        )],
        query       => [qw(
            expires_at
            scopes
        )],
    },

    user_delete_token => {
        method      => "DELETE",
        path        => "/users/<uid>/impersonation_tokens/<tokenid>",
    },

    # Block/unblock
    user_block      => {
        method      => "POST",
        path        => "/users/<uid>/block",
    },

    user_unblock    => {
        method      => "POST",
        path        => "/users/<uid>/unblock",
    },

    # User activities
    user_activities => {
        method      => "GET",
        path        => "/user/activities",
        paginated   => 1,
    },

    # User memberships
    user_memberships => {
        method      => "GET",
        path        => "/users/<uid>/memberships",
        paginated   => 1,
        query => [qw{
            type
        }],
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Users - implements user API calls

See L<GitLab API -- Users|http://doc.gitlab.com/ce/api/users.html> for details and
response formats.

=head1 VERSION

Implements API calls for GitLab CE C<v10.4.1>.

=head1 SYNOPSIS

    use GitLab::API;
    use GitLab::Users;
    # all calls from GitLab::Users are "injected" into the GitLab::API as methods

    my $api = GitLab::API->new();

    my $john = $api->users(username => "john_doe");

    $api->user_create(
        email       => "jane34@somewhere.com",
        password    => "s3cre1",
        username    => "jane.doe",
        name        => "Jane Doe"
    );

=head1 DESCRIPTION

=head2 Notation

All methods take a hash of arguments as specified in the documentation for
L<GitLab::API/exec_request>, but to simplify the documentation,
we will use the following notation:

    users( :username, :search )

which means that the C<users> method takes a hash with two keys, C<username>
and C<search>, so it can be called as

    $gitlab->users(search => "john_doe");

Optional arguments are denoted as C< [:username] >.
Note that not all optional arguments are listed.
Please refer to the official documentation for the full list.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
