package GitLab::Namespaces;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    namespaces => {
        method      => "GET",
        path        => "/namespaces",
        query       => [qw(
            search
            owned_only
            top_level_only
        )],
        paginated   => 1,
    },

    namespace_by_id => {
        method      => "GET",
        path        => "/namespaces/<id>",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Namespaces - extension for Namespaces API

See L<GitLab API -- Namespaces|http://doc.gitlab.com/ce/api/namespaces.html> for details and response formats.

=head1 SYNOPSIS

    use GitLab::API;
    use GitLab::Namespaces;

    my $api = GitLab::API->new();

    # get namespaces exposed to user john.doe
    $api->sudo("john.doe");

    my $namespaces = $api->namespaces(search => "group1");
    my $namespace  = $api->namespace_by_id(id => 10);

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
