package GitLab::Commits;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v11.4.4;

my $requests = {
    commits => {
        method      => "GET",
        path        => "/projects/<pid>/repository/commits",
        query       => [qw(
            ref_name
            since
            until
            path
            all
            with_stats
        )],

        paginated   => 1,
    },

    commit_get => {
        method      => "GET",
        path        => "/projects/<pid>/repository/commits/<sha>",
        query       => [qw(
            stats
        )],
    },

    get_references => {
        method      => "GET",
        path        => "/projects/<pid>/repository/commits/<sha>/refs",
        query       => [qw(
            type
        )],
    },

    cherry_pick => {
        method      => "POST",
        path        => "/projects/<pid>/repository/commits/<sha>/cherry_pick",
        required    => [qw(
            branch
        )],
    },

    commit_revert => {
        method      => "POST",
        path        => "/projects/<pid>/repository/commits/<sha>/revert",
        required    => [qw(
            branch
        )],
    },

    commit_diff => {
        method      => "GET",
        path        => "/projects/<pid>/repository/commits/<sha>/diff",
    },

    commit_comments => {
        method      => "GET",
        path        => "/projects/<pid>/repository/commits/<sha>/comments",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Commits - implements commits API calls

See L<GitLab API -- Commits|https://docs.gitlab.com/ee/api/commits.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
