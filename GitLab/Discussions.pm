package GitLab::Discussions;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any qw($log);

our $VERSION = v11.4.4;

my $requests = {
	"<CLASS>_<ENTITY>_discussions" => {
	    method      => "GET",
	    path        => "/<CLASS>s/<id>/<PREFIX><ENTITY>s/<iid>/discussions",
	},
};

sub import {
    $log->debug("initializing " . __PACKAGE__);

    my $emap = [
        { class => 'group', entity => 'epic', prefix => '', },
        { class => 'project', entity => 'commit', prefix => 'repository/', },
        { class => 'project', entity => 'issue', prefix => '' },
        { class => 'project', entity => 'merge_request', prefix => '' },
        { class => 'project', entity => 'snippet', prefix => '' },
    ];

    while (my ($name, $tmpl) = each(%$requests)) {
        foreach my $instance (@$emap) {
            my $copy = { %$tmpl };

            $copy->{name} //= $name;

            foreach my $v (values %$copy) {
                foreach my $key (keys $instance->%*) {
                    my $placeholder = uc $key;
                    $v =~ s/<$placeholder>/$instance->{$key}/g;
                }
            }

            GitLab::API->register($copy);
        }
    }
}

1;
