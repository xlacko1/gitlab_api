package GitLab::Jobs;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    project_jobs => {
        method  => "GET",
        path    => "/projects/<pid>/jobs",
        query   => [qw(
            scope
        )],
        paginated => 1,
    },

    pipeline_jobs => {
        method  => "GET",
        path    => "/projects/<pid>/pipelines/<pipeline_id>/jobs",
        query   => [qw(
            scope
        )],
        paginated => 1,
    },

    job => {
        method  => "GET",
        path    => "/projects/<pid>/jobs/<job_id>",
    },

#   get_artifacts
#   download_artifacts
#   get_trace_file

    job_cancel => {
        method  => "POST",
        path    => "/projects/<pid>/jobs/<job_id>/cancel",
    },

    job_retry => {
        method  => "POST",
        path    => "/projects/<pid>/jobs/<job_id>/retry",
    },

    job_erase => {
        method  => "POST",
        path    => "/projects/<pid>/jobs/<job_id>/erase",
    },

    keep_artifacts => {
        method  => "POST",
        path    => "/projects/<pid>/jobs/<job_id>/artifacts/keep",
    },

    job_play => {
        method  => "POST",
        path    => "/projects/<pid>/jobs/<job_id>/play",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Jobs - implements jobs API calls

See L<GitLab API -- Jobs|https://docs.gitlab.com/ee/api/jobs.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
