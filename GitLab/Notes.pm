use utf8;
use strict;
use warnings;
use vars qw($VERSION);

package GitLab::Notes;

use GitLab::API;
use Log::Any qw($log);

our $VERSION = v15.8;

my $requests = {
    "<CLASS>_<ENTITY>_notes" => {
        method => "GET",
        path => "/<CLASS>s/<id>/<ENTITY>s/<iid>/notes",
        paginated => 1,
        optional => [qw(
            sort
            order_by
        )],
    },

    "<CLASS>_<ENTITY>_note" => {
        method => 'GET',
        path => '/<CLASS>s/<id>/<ENTITY>s/<iid>/notes/<nid>',
    },

    "<CLASS>_<ENTITY>_note_create" => {
        method => 'POST',
        path => '/<CLASS>s/<id>/<ENTITY>s/<iid>/notes',
        required => [qw(
            body
        )],
        optional => [qw(
            internal
            created_at
        )],
    },

    "<CLASS>_<ENTITY>_note_update" => {
        method => 'PUT',
        path => '/<CLASS>s/<id>/<ENTITY>s/<iid>/notes/<nid>',
        optional => [qw(
            body
        )],
    },

    "<CLASS>_<ENTITY>_note_delete" => {
        method => 'DELETE',
        path => '/<CLASS>s/<id>/<ENTITY>s/<iid>/notes/<nid>',
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);

    my $emap = [
        { class => 'project', entity => 'issue' },
        { class => 'project', entity => 'snippet' },
        { class => 'project', entity => 'merge_request' },
        { class => 'group', entity => 'epic' },
    ];

    while (my ($name, $tmpl) = each(%$requests)) {
        foreach my $instance (@$emap) {
            my $copy = { %$tmpl };

            $copy->{name} //= $name;

            foreach my $v (values %$copy) {
                $v =~ s/<ENTITY>/$instance->{entity}/g;
                $v =~ s/<CLASS>/$instance->{class}/g;
            }

            GitLab::API->register($copy);
        }
    }
}

1;
