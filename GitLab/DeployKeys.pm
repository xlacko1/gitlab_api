package GitLab::DeployKeys;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any qw($log);

our $VERSION = v13.4.1;

my $requests = {
    deploy_keys => {
        method => 'GET',
        path => '/deploy_keys',
    },

    project_deploy_keys => {
        method => 'GET',
        path => '/projects/<id>/deploy_keys',

    },

    project_deploy_key_get => {
        method => 'GET',
        path => '/projects/<id>/deploy_keys/<key_id>',
    },

    project_deploy_key_add => {
        method => 'POST',
        path => '/projects/<id>/deploy_keys',
        required => [qw(
            title
            key
        )],
        optional => [qw(
            can_push
        )],
    },

    project_deploy_key_update => {
        method => 'PUT',
        path => '/projects/<id>/deploy_keys/<key_id>',
        optional => [qw(
            title
            can_push
        )],
    },

    project_deploy_key_delete => {
        method => 'DELETE',
        path => '/projects/<id>/deploy_keys/<key_id>',
    },

    project_deploy_key_enable => {
        method => 'POST',
        path => '/projects/<id>/deploy_keys/<key_id>/enable',
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Projects - implements projects API calls

See L<GitLab API -- Projects|http://doc.gitlab.com/ce/api/projects.html> for details and
response formats.

=head1 VERSION

Implements API calls for GitLab CE C<v10.4.1>.

=head1 DESCRIPTION

=head2 Notation

Please see the documentation for the L<GitLab::Users> module.

Note that not all optional arguments are listed.
Please refer to the official documentation for the full list.

=head2 Project listing

=over

=item L<projects()|https://docs.gitlab.com/ce/api/projects.html#list-projects>

    $gitlab->projects([...]);

Returns a list of projects accessible to the authenticated user.

=item L<project_by_id()|https://docs.gitlab.com/ce/api/projects.html#get-single-project>

    $gitlab->project_by_id( :id );

Returns the project with the given ID, which can be either a number or a string C<"namespace/project_name">.

=item L<project_users()|https://docs.gitlab.com/ce/api/projects.html#get-project-users>

B<This method has been removed from API>.

Use L<GitLab::Members::project_members()|GitLab::Members/project_members()> instead.

=item L<project_events()|https://docs.gitlab.com/ce/api/projects.html#get-project-events>

This method has been moved to
L<GitLab::Events::project_evens()|GitLab::Events/project_events()>.

=back

=head2 Create, edit and delete projects

=over

=item L<project_create()|https://docs.gitlab.com/ce/api/projects.html#create-project>

    $gitlab->project_create( (:name|:path) [...] );

Creates a new project in the authenticated user's namespace.
Other namespaces can be specified by the optional C<namespace_id> parameter if the user can access it.

=item L<project_create_for_user()|https://docs.gitlab.com/ce/api/projects.html#create-project-for-user>

    $gitlab->project_create_for_user( :uid, :name );

Creates a new project for the user with the given C<uid>.
B<This method is available only for administrators.>

=item L<project_edit()|https://docs.gitlab.com/ce/api/projects.html#edit-project>

    $gitlab->project_edit( :id, [...] );

Edits the project.
Takes the same arguments as L<project_create>, except they are all optional.

=item L<project_fork()|https://docs.gitlab.com/ce/api/projects.html#fork-project>

    $gitlab->project_fork( :id, [:namespace ] );

Forks the project to the authenticated user's namespace or the namespace specified by the C<namespace> as an id or path.

=item L<project_star()|https://docs.gitlab.com/ce/api/projects.html#star-a-project>

    $gitlab->project_star( :id );

Stars a given project.
Returns C<304 Not Modified> if already stared.

=item L<project_unstar()|https://docs.gitlab.com/ce/api/projects.html#unstar-a-project>

    $gitlab->project_unstar( :id );

Unstars a given project.
Returns C<304 Not Modified> if not stared.

=item L<project_archive()|https://docs.gitlab.com/ce/api/projects.html#archive-a-project>

    $gitlab->project_archive( :id );

Archives the given project.
The user must be either an administrator or the owner of the project.

=item L<project_unarchive()|https://docs.gitlab.com/ce/api/projects.html#unarchive-a-project>

    $gitlab->project_unarchive( :id );

Unarchives the given project.
The user must be either an administrator or the owner of the project.

=item L<project_delete()|https://docs.gitlab.com/ce/api/projects.html#remove-project>

    $gitlab->project_delete( :id );

Deletes the project with the given C<id>.

=back

=head2 Uploads

=over

=item L<project_upload_file()|https://docs.gitlab.com/ce/api/projects.html#upload-a-file>

B<NOT SUPPORTED YET>

=back

=head2 Project Members

See L<GitLab::Members> module.

=head2 Share project with a group

=over

=item L<project_share()|https://docs.gitlab.com/ce/api/projects.html#share-project-with-group>

    $gitlab->project_share( :id, :group_id, :group_access, [:expires_at] );

Allow to share project with group.

=item L<project_unshare()|https://docs.gitlab.com/ce/api/projects.html#delete-a-shared-project-link-within-a-group>

    $gitlab->project_unshare( :id, :group_id );

Unshare the project from the group. Returns C<204 No Content> on success.

=back

=head2 Hooks

=over

=item L<project_hooks()|https://docs.gitlab.com/ce/api/projects.html#list-project-hooks>

    $gitlab->project_hooks( :id );

Get a list of project hooks.

=item L<project_get_hook()|https://docs.gitlab.com/ce/api/projects.html#get-project-hook>

    $gitlab->project_get_hook( :id, :hook_id )

Get a specific hook for a project.

=item L<project_add_hook()|https://docs.gitlab.com/ce/api/projects.html#add-project-hook>

    $gitlab->project_add_hook( :id, :url, [...]);

Adds a hook to a specified project.

=item L<project_edit_hook()|https://docs.gitlab.com/ce/api/projects.html#edit-project-hook>

    $gitlab->project_edit_hook( :id, :hook_id, [...]);

Edits a hook for a specified project.

=item L<project_delete_hook()|https://docs.gitlab.com/ce/api/projects.html#delete-project-hook>

    $gitlab->project_delete_hook( :id, :hook_id );

Removes a hook from a project.
This is an idempotent method and can be called multiple times.

Note the JSON response differs if the hook is available or not.
If the project hook is available before it is returned in the JSON response or an empty response is returned.

=back

=head2 Branches

See L<GitLab::Branches> module.

=head2 Miscellaneous

=over

=item L<project_create_forked_relationship()|https://docs.gitlab.com/ce/api/projects.html#create-a-forked-from-to-relation-between-existing-projects>

    $gitlab->project_create_forked_relationship( :id :forked_from_id );

Create a I<forked from> relation between existing projects.
B<Available only for admins.>

=item L<project_delete_forked_relationship()|https://docs.gitlab.com/ce/api/projects.html#delete-an-existing-forked-from-relationship>

    $gitlab->project_delete_forked_relationship( :id );

Delete an existing I<forked from> relationship.

=ítem L<project_start_housekeeping()|https://docs.gitlab.com/ce/api/projects.html#start-the-housekeeping-task-for-a-project>

    $gitlab->project_start_housekeeping( :id );

Starts project housekeeping (essentially calls C<git gc> in the project repository).

=back

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back

