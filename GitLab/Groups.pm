package GitLab::Groups;

use utf8;
use strict;
use warnings;
use vars qw($VERSION);

use GitLab::API;
use Log::Any        qw($log);

our $VERSION = v10.4.1;

my $requests = {
    groups  => {
        method      => "GET",
        path        => "/groups",
        query       => [qw(
            skip_groups
            all_available
            search
            order_by
            sort
            statistics
            owned
        )],
        paginated   => 1,
    },

    subgroups => {
        method      => "GET",
        path        => "/groups/<gid>/subgroups",
        query       => [qw(
            skip_groups
            all_available
            search
            order_by
            sort
            statistics
            owned
        )],
        encode      => [qw(
            gid
        )],
        paginated   => 1,
    },

    group_get_projects => {
        method      => "GET",
        path        => "/groups/<gid>/projects",
        optional    => [qw(
            archived
            visibility
            order_by
            sort
            search
            simple
            owned
            starred
        )],
        encode      => [qw(
            gid
        )],
        paginated   => 1,
    },

    group_details => {
        method      => "GET",
        path        => "/groups/<gid>",
        encode      => [qw(
            gid
        )],
    },

    group_create => {
        method      => "POST",
        path        => "/groups",
        required    => [qw(
            name
            path
        )],
        optional    => [qw(
            description
            visibility
            lfs_enabled
            request_access_enabled
            parent_id
            shared_runners_minutes_limit
        )],
    },

    transfer_project_to_group => {
        method      => "POST",
        path        => "/groups/<gid>/projects/<projid>",
    },

    group_update => {
        method      => "PUT",
        path        => "/groups/<gid>",
        optional    => [qw(
            name
            path
            description
            visibility
            lfs_enabled
            request_access_enabled
        )],
    },

    group_delete    => {
        method      => "DELETE",
        path        => "/groups/<gid>",
    },
};

sub import {
    $log->debug("initializing " . __PACKAGE__);
    while (my ($name, $tmpl) = each(%$requests)) {
        $tmpl->{name} = $name unless exists $tmpl->{name};
        GitLab::API->register($tmpl);
    }
}

1;

__END__

=head1 NAME

GitLab::Groups - implements group API calls

See L<GitLab API -- Groups|http://doc.gitlab.com/ce/api/groups.html> for details and
response formats.

=head1 AUTHOR

Roman Lacko <L<xlacko1@fi.muni.cz>>

=head1 SEE ALSO

=over

=item L<GitLab>

Wrapper around L<GitLab::API> and other C<GitLab::*> modules.

=back
